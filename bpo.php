<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.01 Transitional//EN">
<html>
<head>
<title>Fruzsi Corp Blueprints</title>
</head>
<body>


<table>
<tr>
<td>Név</td>
<td>típus</td>
<td>Anyag</td>
<td>Idő</td>
</tr>
<?php
$bpos = array();
$dom = new DOMDocument;
$eveApiKeys = array(  
                     array( 'keyID' => 3836735
                           ,'vCode' => 'Z8jxOshEHXmv3aTxVH0l8sLQpZoAkUEjQUcRi25B2n1H8WYKZ1XLJMyPg9N4o2Eq'
                           ,'characterID' => '95001353'  
                           )
                    ,array( 'keyID' => 4806236
                           ,'vCode' => 'bxT5opaIEVO9fHMosysB3Z2QvAi2ggVMYoNgQbhHtEBlYKhJl2F9nYAA39F4GZeA'
                           ,'characterID' => '95448173'  
                           )
                   );

function getOwnedBlueprintsUrl($keyID, $vCode, $characterID) {
  return $bpsUrl = 'https://api.eveonline.com/char/Blueprints.xml.aspx?keyID='
         . $keyID . '&vCode=' . $vCode . '&characterID=' . $characterID;
}

function loadBlueprintsXmlRow($dom)
{
    $blueprints = $dom->getElementsByTagName('row');
    foreach ($blueprints as $bp) {
        $bpName = $bp->getAttribute('typeName');
        if ($bp->getAttribute('runs') == -1) {$bpRun = 'N/A';} else {$bpRun = $bp->getAttribute('runs');};
        $r = $bpos[$bpName]['Run'];
        if ($r != 'N/A') {
           $bpos[$bpName] = array('Name' => $bpName 
                                 ,'ME' => $bp->getAttribute('materialEfficiency')
                                 ,'TE' => $bp->getAttribute('timeEfficiency')
                                 ,'Run' => $bpRun)
                                 ;
       }
    }
    return $bpos;  
} 
  
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

$xml = <<< XML
<?xml version='1.0' encoding='UTF-8'?>
<eveapi version="2">
  <currentTime>2015-12-29 08:17:11</currentTime>
  <result>
    <rowset name="blueprints" key="itemID" columns="itemID,locationID,typeID,typeName,flagID,quantity,timeEfficiency,materialEfficiency,runs">
      <row itemID="1016816099468" locationID="1018477161637" typeID="11301111" typeName="A TESZT Blueprint" flagID="0" quantity="-1" timeEfficiency="20" materialEfficiency="10" runs="-1" />
      <row itemID="1016913713327" locationID="1018477161637" typeID="27913" typeName="Concussion Bomb Blueprint" flagID="0" quantity="-1" timeEfficiency="20" materialEfficiency="10" runs="-1" />
      <row itemID="1017006700297" locationID="1018477161637" typeID="27921" typeName="Electron Bomb Blueprint" flagID="0" quantity="-1" timeEfficiency="20" materialEfficiency="10" runs="-1" />
      <row itemID="1017007022267" locationID="1018477161637" typeID="27917" typeName="Scorch Bomb Blueprint" flagID="0" quantity="-1" timeEfficiency="20" materialEfficiency="10" runs="-1" />
      <row itemID="1017123229159" locationID="1018477161637" typeID="27919" typeName="Shrapnel Bomb Blueprint" flagID="0" quantity="-1" timeEfficiency="20" materialEfficiency="10" runs="-1" />
      <row itemID="1019391899906" locationID="60001006" typeID="790" typeName="Torpedo Launcher I Blueprint" flagID="4" quantity="-2" timeEfficiency="0" materialEfficiency="0" runs="1" />
      <row itemID="1019391899907" locationID="60001006" typeID="790" typeName="Torpedo Launcher I Blueprint" flagID="4" quantity="-2" timeEfficiency="0" materialEfficiency="0" runs="1" />
      <row itemID="1019391992596" locationID="60001006" typeID="12041" typeName="Purifier Blueprint" flagID="4" quantity="-2" timeEfficiency="12" materialEfficiency="3" runs="3" />
      <row itemID="1019404753002" locationID="60001006" typeID="33077" typeName="Small Ancillary Armor Repairer Blueprint" flagID="4" quantity="-2" timeEfficiency="0" materialEfficiency="0" runs="25" />
      <row itemID="1019406453589" locationID="60001006" typeID="16239" typeName="Cormorant Blueprint" flagID="4" quantity="-2" timeEfficiency="0" materialEfficiency="0" runs="20" />
      <row itemID="1019410537597" locationID="60001006" typeID="4251" typeName="Small Tractor Beam II Blueprint" flagID="4" quantity="-2" timeEfficiency="4" materialEfficiency="2" runs="10" />
      <row itemID="1019410538610" locationID="60001006" typeID="950" typeName="Merlin Blueprint" flagID="4" quantity="-2" timeEfficiency="18" materialEfficiency="9" runs="30" />
      <row itemID="1019410538611" locationID="60001006" typeID="950" typeName="Merlin Blueprint" flagID="4" quantity="-2" timeEfficiency="18" materialEfficiency="9" runs="30" />
    </rowset>
  </result>
  <cachedUntil>2015-12-29 19:47:11</cachedUntil>
</eveapi>
XML;

$bpsUrl = 'https://api.eveonline.com/char/Blueprints.xml.aspx?keyID=4806236&vCode=bxT5opaIEVO9fHMosysB3Z2QvAi2ggVMYoNgQbhHtEBlYKhJl2F9nYAA39F4GZeA&characterID=95448173';

foreach ($eveApiKeys as $apiKey) {
    $dom->load(getOwnedBlueprintsUrl($apiKey['keyID'], $apiKey['vCode'], $apiKey['characterID']));
    $bpos = array_merge($bpos, loadBlueprintsXmlRow($dom));
    // print count($bpos). '<br/>';
} 
/*
$dom->loadXML($xml);
loadBlueprintsXmlRow($dom);
$bpos = array_merge($bpos, loadBlueprintsXmlRow($dom));
*/
// print count($bpos). '<br/>';


$sorted = array_orderby($bpos, 'Name', SORT_ASC);
foreach ($sorted as $bp) {
    $bpName= $bp['Name'];
    $bpME  = $bp['ME'];
    $bpTE  = $bp['TE'];
    $bpRun = $bp['Run'];
    print "<tr><td>$bpName</td><td>$bpRun</td><td>$bpME</td><td>$bpTE</td></tr>";
}
?>

</table>

</body>
</html>

