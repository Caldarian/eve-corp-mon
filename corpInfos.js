corpInfo = {
	id: 98429458, 
	name: "Fruzsi Corp.",
	keyID: "-------",
	vCode: "----------------------------------------------------",
	characterID: "",
	itemsID: "",
	generateApiUrl: function( apiServiceName ) {
		return "https://api.eveonline.com/corp/" + apiServiceName 
		    + ".xml.aspx?keyID=" + this.keyID 
			+ "&vCode=" + this.vCode
			+ "&characterID=" + this.characterID
			+ "&ids=" + this.itemsID;
	},
	getLogoUrl: function() {
		return "https://image.eveonline.com/Corporation/" + this.id + "_64.png";
	},
	getCorpInfoStr: function() {
		return '<img src="' + this.getLogoUrl() + '"><br />'+ this.name;
	},
}
