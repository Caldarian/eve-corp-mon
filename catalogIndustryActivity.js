var eveIndustryActivity =  { 
  1 : {name: "Prod" },
  3 : {name: "TE"   },
  4 : {name: "ME"   },
  5 : {name: "Copy" },
  8 : {name: "Inv"  },
};

var IndustryActivityHRType = 0;

function industryActivityHR( activityId ) {
  return eveIndustryActivity[activityId]['name'];
}

function industryActivityHtmlClass( activityId ) {
  if (activityId == 1) { return "industryProduction" ;}	
  if (activityId == 3) { return "industryTEResearch" ;}	
  if (activityId == 4) { return "industryMEResearch" ;}	
  if (activityId == 5) { return "industryCopy" ;}	
}

