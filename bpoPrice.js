console.log('bpoPrice: Start');

function loadChrukerBpoXMLDoc() {
  var bpId = document.getElementById("itemId").value;
  document.getElementById("apiUrl").innerHTML = bpoUrl(bpId);
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        processBpoSite(xmlhttp, bpId);
    }
  };
  xmlhttp.open("GET", bpoUrl(bpId), true);
  xmlhttp.send();
}

function bpoUrl(bpoTypeId) {
  return 'http://games.chruker.dk/eve_online/item.php?type_id=' + bpoTypeId; 
} 

function processBpoSite(xml, bpTypeId) {
  var i;
  var siteSource = xml.responseText;
  var basePriceRegExp = /(Baseprice: )([0-9,]*)( ISK)/i
//  console.log(siteSource.search(basePriceRegExp));
//  var p = siteSource.search(basePriceRegExp) + 11;
//  var priceStr = siteSource.substr(p, 30); 
//  console.log(priceStr);
  var match = basePriceRegExp.exec(siteSource); 
//  console.log(match[2]);
  var priceStr = match[2];
  var price = priceStr.replace(/,/g,"");
	 
  var tablaBoby="<tr><th>Price</th></tr>";
  tablaBoby += '<tr>' + addTableData(price) + "</tr>\n";
  document.getElementById("resultTable").innerHTML = tablaBoby;
}

