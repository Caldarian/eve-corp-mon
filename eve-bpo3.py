#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys
import xml.etree.ElementTree as ET

global cur

##con = None
##tree= None
##root= None
##cur = None

def stdout(str):
    ## print(str)
    print str


##con = lite.connect('test.db')
##with con:    
##    cur = con.cursor()    
##    cur.execute("SELECT * FROM Cars")
##    rows = cur.fetchall()
##    for row in rows:
##        print row


    

def bluePrint(node, nodePathStr):
    ## stdout('    '+ nodePathStr+ " " + node.attrib['id']+ " " + node.attrib['name'])
    bpMaterialInsertSql = 'INSERT INTO bpMaterials(bp_id,material_id,quantity) VALUES (?,?,?)'
    bpSkillInsertSql = 'INSERT INTO bpSkills(bp_id,skill_id,skill_level) VALUES (?,?,?)'
    bpInventionInsertSql = 'INSERT INTO bpInventions(base_bp_id,inveted_bp_id,chance) VALUES (?,?,?)'
    bpInsertSql = """
        INSERT INTO blueprints(id, name, icon, metagroup,productTypeId
        ,productionTime, researchProductivityTime, researchMaterialTime
        ,researchCopyTime, reverseEngineeringTime, inventionTime
        ,maxProductionLimit, hiearchy) VALUES 
        """
    bpInsertSql = bpInsertSql + "(" + node.attrib['id'] 
    bpInsertSql = bpInsertSql + ',"' + node.attrib['name'] + '" '
    bpInsertSql = bpInsertSql + ",0 "
    bpInsertSql = bpInsertSql + ",'" + node.attrib['metaGroup'] + "' "
    bpInsertSql = bpInsertSql + "," + node.attrib['productTypeID']
    bpInsertSql = bpInsertSql + "," + node.attrib['productionTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['researchProductivityTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['researchMaterialTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['researchCopyTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['reverseEngineeringTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['inventionTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['maxProductionLimit']
    bpInsertSql = bpInsertSql + ",'" + nodePathStr + "')"
    # stdout(bpInsertSql)
    cur.execute(bpInsertSql)

    for subnode in node:
        if subnode.tag == 's':
            # stdout(subnode.attrib['id']+'+'+subnode.attrib['lv'])
            cur.execute(bpSkillInsertSql, (node.attrib['id'], subnode.attrib['id'], subnode.attrib['lv']))
        if subnode.tag == 'm':
            # stdout(subnode.attrib['id']+'+'+subnode.attrib['quantity'])
            cur.execute(bpMaterialInsertSql, (node.attrib['id'], subnode.attrib['id'], subnode.attrib['quantity']))
        if subnode.tag == 'inventTypeIDs':
            for inventNode in subnode:
                # stdout(inventNode.tag+'+'+inventNode.attrib['double']+'+'+inventNode.text)
                cur.execute(bpInventionInsertSql, (node.attrib['id'], int(inventNode.text), inventNode.attrib['double']))

def subgroup(node, nodePathStr):
    if len(node) > 0:
        for subnode in node:
            pStr = nodePathStr+ '.'+subnode.attrib['name']
            for subsubnode in subnode:
                if subsubnode.tag == 'subGroups':
                   subgroups(subsubnode, pStr)
                if subsubnode.tag == 'blueprint':
                   bluePrint(subsubnode, pStr)

def subgroups(node, nodePathStr):
    if len(node) > 0:  
        if node.tag == 'subGroups':
            subgroup(node, nodePathStr)

def createBPODatabase():
    cur.executescript("""
        DROP TABLE IF EXISTS blueprints;
        CREATE TABLE blueprints(id INT
					,name VARCHAR(128)
					,icon INT
					,metagroup VARCHAR(2)
					,productTypeId INT
					,productionTime INT 
					,researchProductivityTime INT
					,researchMaterialTime INT 
					,researchCopyTime INT 
					,reverseEngineeringTime INT 
					,inventionTime INT 
					,maxProductionLimit INT 
					,hiearchy VARCHAR(255)
					);
					
        DROP TABLE IF EXISTS bpSkills;
        CREATE TABLE bpSkills(bp_id INT
					,skill_id INT
					,skill_name VARCHAR(128)
					,skill_level INT
					,activity_id INT
					);
        DROP TABLE IF EXISTS bpInventions;
        CREATE TABLE bpInventions(base_bp_id INT
					,inveted_bp_id INT
					,chance REAL
					);
        DROP TABLE IF EXISTS bpMaterials;
        CREATE TABLE bpMaterials(bp_id INT
                                 ,material_id INT
                                 ,material_name VARCHAR(128)
	                         ,quantity INT
                                 ,activityId INT
                                 );
        """)

def main():
    # try:
    global cur
    con = lite.connect('bpo.db')
    cur = con.cursor()
    createBPODatabase()
    con.commit()
    tree = ET.parse('eve-blueprints-en-US.xml')
    # tree = ET.parse("C:\\Users\\Zoli\\Dropbox\\eve\\oszk\\eve-blueprints-en-US.xml")
    root = tree.getroot()
    for child in root:
        stdout(child.attrib['name'])
        subgroups(child[0], child.attrib['name'])
        con.commit()
        
    ##except lite.Error, e:
    ##    print "Error %s:" % e.args[0]
    ##    sys.exit(1)
    ##    
    ##finally:
    if con:
        con.close()

if __name__ == '__main__':
    main()
