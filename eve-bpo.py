#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys
import xml.etree.ElementTree as ET

def stdout(str):
    print(str)
    ## print str
    

def bluePrint(node, nodePathStr):
    ## stdout('    '+ nodePathStr+ " " + node.attrib['id']+ " " + node.attrib['name'])
    bpInsertSql = """
        INSERT INTO blueprints(id, ,name, icon, metagroup,productTypeId
					,productionTime, researchProductivityTime, researchMaterialTime
					,researchCopyTime, reverseEngineeringTime, inventionTime
					,maxProductionLimit, hiearchy) VALUES (
        """
    bpInsertSql = bpInsertSql + "(" + node.attrib['id'] 
    bpInsertSql = bpInsertSql + ",'" + node.attrib['name'] + "' "
    bpInsertSql = bpInsertSql + ",0 "
    bpInsertSql = bpInsertSql + ",'" + node.attrib['metaGroup'] + "' "
    bpInsertSql = bpInsertSql + "," + node.attrib['productTypeId']
    bpInsertSql = bpInsertSql + "," + node.attrib['researchProductivityTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['researchMaterialTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['researchCopyTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['reverseEngineeringTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['inventionTime']
    bpInsertSql = bpInsertSql + "," + node.attrib['maxProductionLimit']
    bpInsertSql = bpInsertSql + ",'" + nodePathStr + "')"
    stdout(bpInsertSql)
    cur.execute(bpInsertSql)
  
def subgroup(node, nodePathStr):
    if len(node) > 0:
        for subnode in node:
            aaa = nodePathStr+ '.'+subnode.attrib['name']
            stdout(aaa)
            for subsubnode in subnode:
                if subsubnode.tag == 'subGroups':
                   subgroups(subsubnode, aaa)
                if subsubnode.tag == 'blueprint':
                   bluePrint(subsubnode, aaa)

def subgroups(node, nodePathStr):
    if len(node) > 0:  
        if node.tag == 'subGroups':
            subgroup(node, nodePathStr)

## tree = ET.parse('eve-blueprints-en-US.xml')
## root = tree.getroot()
## root = ET.fromstring(country_data_as_string)
## root.tag
## root.attrib {}
##for child in root:
##    if child.attrib['name'] == 'Various Non-Market': 
##        exit()
##    subgroups(child[0], child.attrib['name'])

def createBPODatabase():
    cur.executescript("""
        DROP TABLE IF EXISTS blueprints;
        CREATE TABLE blueprints(id INT
					,name VARCHAR(128)
					,icon INT
					,metagroup VARCHAR(2)
					,productTypeId INT
					,productionTime INT 
					,researchProductivityTime INT
					,researchMaterialTime INT 
					,researchCopyTime INT 
					,reverseEngineeringTime INT 
					,inventionTime INT 
					,maxProductionLimit INT 
					,hiearchy VARCHAR(255)
          PRIMERY KEY (id)
					);
        CREATE TABLE bpSkills(bp_id INT
					,skill_id INT
					,skill_name VARCHAR(128)
					,skill_level INT
          ,activity_id INT
					);
        CREATE TABLE bpInventions(base_bp_id INT
          ,inveted_bp_id INT
					,chance REAL
          PRIMERY KEY (base_bp_id, inveted_bp_id)
					);
        CREATE TABLE bpMaterials(bp_id INT
					,material_id INT
					,material_name VARCHAR(128)
					,quantity INT
          ,activityId INT
          PRIMERY KEY (bp_id, material_id)
					);
        """)

def main():
    con = None
    # try:
    tree = ET.parse('eve-blueprints-en-US.xml')
    root = tree.getroot()
    con = lite.connect('bpo.db')
    cur = con.cursor()
    createBPODatabase()
    con.commit()
    tree = ET.parse('eve-blueprints-en-US.xml')
    # tree = ET.parse("C:\\Users\\Zoli\\Dropbox\\eve\\oszk\\eve-blueprints-en-US.xml")
                     
    root = tree.getroot()
    for child in root:
       ## if child.attrib['name'] == 'Various Non-Market': exit()
       subgroups(child[0], child.attrib['name'])
        
    ##except lite.Error, e:
    ##    print "Error %s:" % e.args[0]
    ##    sys.exit(1)
    ##    
    ##finally:
    if con:
        con.close()

if __name__ == '__main__':
    main()
