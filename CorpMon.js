console.log('CorpMon: Start');

function onLoadBody() {
  document.getElementById("CorpInformations").innerHTML = corpInfo.getCorpInfoStr();
  initCorpApiCallsComboBox();
}

function initCorpApiCallsComboBox() { 
  var corpApiCalls = '<option value="Bookmarks">Bookmarks</option><option value="Blueprints">Blueprints</option>';
  corpApiCalls += '<option value="MemberTrackingExtended">MemberTrackingExtended</option>';
  corpApiCalls += '<option value="Locations">Locations</option>';
  corpApiCalls += '<option value="Contracts">Contracts</option>';
  corpApiCalls += '<option value="Titles">Titles</option>';
  corpApiCalls += '<option value="WalletTransactions">WalletTransactions</option>';
  corpApiCalls += '<option value="WalletJournal">WalletJournal</option>';
  corpApiCalls += '<option value="StarbaseList">StarbaseList</option>';
  corpApiCalls += '<option value="Standings">Standings</option>';
  corpApiCalls += '<option value="StarbaseDetail">StarbaseDetail</option>';
  corpApiCalls += '<option value="Shareholders">Shareholders</option>';
  corpApiCalls += '<option value="OutpostServiceDetail">OutpostServiceDetail</option>';
  corpApiCalls += '<option value="OutpostList">OutpostList</option>';
  corpApiCalls += '<option value="Medals">Medals</option>';
  corpApiCalls += '<option value="MarketOrders">MarketOrders</option>';
  corpApiCalls += '<option value="MemberTrackingLimited"></option>';
  corpApiCalls += '<option value="MemberSecurityLog">MemberSecurityLog</option>';
  corpApiCalls += '<option value="MemberSecurity">MemberSecurity</option>';
  corpApiCalls += '<option value="KillLog">KillLog</option>';
  corpApiCalls += '<option value="IndustryJobs">IndustryJobs</option>';
  corpApiCalls += '<option value="FacWarStats">FacWarStats</option>';
  corpApiCalls += '<option value="ContainerLog">ContainerLog</option>';
  corpApiCalls += '<option value="ContactList">ContactList</option>';
  corpApiCalls += '<option value="CorporationSheet">CorporationSheet</option>';
  corpApiCalls += '<option value="MemberMedals">MemberMedals</option>';
  corpApiCalls += '<option value="AssetList">AssetList</option>';
  corpApiCalls += '<option value="AccountBalance">AccountBalance</option>';
  document.getElementById("cbxCorporateCalls").innerHTML = corpApiCalls;
}

function hideOrShowResults() {
  var butt = document.getElementById("btnHideResult"); 
  var resSec = document.getElementById("results"); 
  var isHiddenResults = (resSec.style.visibility == "hidden");
  if (!isHiddenResults) {
    butt.innerHTML = "Show Results";
    resSec.style.visibility = "collapse"; // "hidden"; 
    console.log('CorpMon: Show' );
  }
  else {
    butt.innerHTML = "Hide Results"; 
    resSec.style.visibility = "visible"; 
    console.log('CorpMon: Hide' );
  }   
}

function getSelectedApiCall() {
  return document.getElementById("cbxCorporateCalls").value;
}

function dispApiCallResult() {
  corpInfo.characterID = document.getElementById("characterId").value;
  corpInfo.ItemsID = document.getElementById("itemId").value;
  loadXMLDoc(getSelectedApiCall());
}

function loadXMLDoc(serviceName) {
  document.getElementById("apiUrl").innerHTML = corpInfo.generateApiUrl(serviceName);
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
//      var xmlDoc = xmlhttp.responseXML;
//      var x = xmlDoc.getElementsByTagName("error");
//	  if (x.getAttribute('code') > 0) {
//        console.log('CorpMon: API error' + x.getAttribute('code'));
//	  }
//	  else {
        listCorpApiCall(xmlhttp, serviceName);
//	  }
    }
  };
  xmlhttp.open("GET", corpInfo.generateApiUrl(serviceName), true);
  xmlhttp.send();
}

// https://api.eveonline.com/corp/MemberSecurity.xml.aspx?keyID=4838498&vCode=fO15AyT9cznu16baCmLCjLz8jx9ZeDbQVGONr8GGV1Tw61UG3qWE8Od9GzdOHMfD&characterID=

function listCorpApiCall(xml, callName) {
  var tablaBoby; 
  switch (callName) {
    case 'IndustryJobs':
        tablaBoby = listIndustryJobs(xml); // listEveRows(xml, callName); 
        break;
    case 'Blueprints':
        tablaBoby = listBPOs(xml); // listEveRows(xml, callName); 
        break;
    default:
        tablaBoby = listEveRows(xml, callName); 
  }
  document.getElementById("resultTable").innerHTML = tablaBoby;
}

function listBPOs(xml) {
  var i;
  var xmlDoc = xml.responseXML;
  var x = xmlDoc.getElementsByTagName("row");
  var table="<tr><th>Blueprint</th><th>ME</th><th>TE</th><th>Runs</th></tr>";
  var BPColor = "";
  var isDuplicated = false;
  
  // itemID	locationID	typeID	typeName	flagID	quantity	timeEfficiency	materialEfficiency	runs
  // <meter value="2" min="0" max="10">2 out of 10</meter><br>
  // <meter value="0.6">60%</meter>
  for (i = 0; i <x.length; i++) {
    if (i == 0) { 
      isDuplicated = false;
    } 
    else {
      isDuplicated = (x[i].getAttribute('typeName') == x[i-1].getAttribute('typeName'))
        && (x[i].getAttribute('materialEfficiency') == x[i-1].getAttribute('materialEfficiency'))  
        && (x[i].getAttribute('timeEfficiency')     == x[i-1].getAttribute('timeEfficiency'))  
        && (x[i].getAttribute('runs')               == x[i-1].getAttribute('runs'));  
    }   
    if (!isDuplicated) {
      // if (x[i].getAttribute('runs') == -1) {BPColor = "BPO"} else {BPColor = "BPC"};  
      BPColor = (x[i].getAttribute('runs') == -1) ? "BPO" : "BPC";  
      table += '<tr class="' + BPColor + '">'
        + addTableData(x[i].getAttribute('typeName'))
        + addTableData('<meter value="'+x[i].getAttribute('materialEfficiency')+'" min="0" max="10" ></meter> ' + x[i].getAttribute('materialEfficiency'))  
        + addTableData('<meter value="'+x[i].getAttribute('timeEfficiency')+'" min="0" max="20" ></meter> ' + x[i].getAttribute('timeEfficiency'))  
        + addTableDataWithClass(x[i].getAttribute('runs'), 'tableDataNumber')
        "</tr>";
    }  
  }
  return table;
}

function addTableData( dataStr ) {
  return "<td>" + dataStr +  "</td>";
}

function addTableDataWithClass( dataStr, classStr) {
  return '<td class="'+classStr+'">' + dataStr +  "</td>";
}

function addTableCol( colCaptionStr ) {
  return "<th>" + colCaptionStr +  "</th>";
}

function formatItemType( itemTypeId ) {
  return eveItems[itemTypeId]["name"];
}

function formatSecIntervalToHR( secInterval ) {
  
  var d = Math.floor(secInterval / 86400);
  var hms = new Date((secInterval % 86400)* 1000);
  return d + 'd ' + hms.toISOString().substr(11,8);
}

function listIndustryJobs(xml) {
  var i;
  var xmlDoc = xml.responseXML;
  var x = xmlDoc.getElementsByTagName("row");
  // jobID
	// ,installerID,installerName
	// ,facilityID,solarSystemID,solarSystemName,stationID
	// ,activityID
	// ,blueprintID,blueprintTypeID,blueprintTypeName,blueprintLocationID
	// ,outputLocationID,runs,cost,teamID,licensedRuns,probability
    // ,productTypeID,productTypeName,status,timeInSeconds,startDate,endDate
	// ,pauseDate,completedDate,completedCharacterID,successfulRuns
  var table="<tr>"
    + addTableCol('activity')
    + addTableCol('blueprintTypeName')
    + addTableCol('facilityID')
    + addTableCol('stationID')
    + addTableCol('runs')
    + addTableCol('endDate')
    + addTableCol('startDate')
    + addTableCol('licensedRuns')
    + addTableCol('probability')
    + addTableCol('installerName')
    + addTableCol('successfulRuns')
    + addTableCol('timeInSeconds')
    + addTableCol('product')
    + addTableCol('productTypeName')
    + addTableCol('status')
    + addTableCol('cost')
    + addTableCol('blueprintLocationID')
    + addTableCol('outputLocationID')
    +"</tr>";
  for (i = 0; i <x.length; i++) {
    table += '<tr class="' + industryActivityHtmlClass(x[i].getAttribute('activityID')) + '">';
    table += "" 
    + addTableData(industryActivityHR(x[i].getAttribute('activityID')))
    + addTableData(x[i].getAttribute('blueprintTypeName'))
    + addTableData(x[i].getAttribute('facilityID'))
    + addTableData(x[i].getAttribute('stationID'))
    + addTableData(x[i].getAttribute('runs'))
    + addTableData(x[i].getAttribute('endDate'))
    + addTableData(x[i].getAttribute('startDate'))
    + addTableData(x[i].getAttribute('licensedRuns'))
    + addTableData(x[i].getAttribute('probability'))
    + addTableData(x[i].getAttribute('installerName'))
    + addTableData(x[i].getAttribute('successfulRuns'))
    + addTableData(formatSecIntervalToHR(x[i].getAttribute('timeInSeconds')))
    + addTableData(formatItemType(x[i].getAttribute('productTypeID')))
    + addTableData(x[i].getAttribute('productTypeName'))
    + addTableData(x[i].getAttribute('status'))
    + addTableDataWithClass(x[i].getAttribute('cost'), 'tableDataNumber')
    + addTableData(x[i].getAttribute('blueprintLocationID'))
    + addTableData(x[i].getAttribute('outputLocationID'))
    "</tr>";
  }
  return table;
}

function listEveRows(xml, callName) {
  var i;
  var j;
  var xmlDoc = xml.responseXML;
  var x = xmlDoc.getElementsByTagName("rowset");
  var fieldsOfRows = x[0].getAttribute('columns');
  var fields = fieldsOfRows.split(',');
  var table="<tr><th>" + fields.join("</th><th>") + "</th></tr>";
  var x = xmlDoc.getElementsByTagName("row");
  for (j = 0; j < x.length; j++) { 
    table += "<tr><td>"+ x[j].getAttribute(fields[0]);
    for (i = 1; i < fields.length; i++) { 
      table +=  "</td><td>" + x[j].getAttribute(fields[i]);
    }  
    table += "</td></tr>";
  }  
  return table;
}

function loadMineral() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      loadMineralCostFromEveCentral(xmlhttp);
    }
  };
  // xmlhttp.open("GET", "http://api.eve-central.com/api/quicklook?typeid=2073&usesystem=30000142&setminQ=5000&sethours=24", true);
  xmlhttp.open("GET", "http://api.eve-central.com/api/evemon", true);
  xmlhttp.send();
}

function loadMineralCostFromEveCentral(xml) {
  var i;
  var xmlDoc = xml.responseXML;
  var x = xmlDoc.getElementsByTagName("mineral");
  var table="<tr><th>Ásvány</th><th>Ár</th></tr>";
  for (i = 0; i <x.length; i++) { 
    table += "<tr>" 
    + addTableData(x[i].getElementsByTagName('name')[0].childNodes[0].nodeValue) 
    + addTableData(x[i].getElementsByTagName('price')[0].childNodes[0].nodeValue) 
    + "<tr>\n";
  }
  document.getElementById("resultTable").innerHTML = table;
}

function loadOwnedBPO() {
  document.getElementById("apiUrl").innerHTML = corpInfo.generateApiUrl('Blueprints');
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      listBPOLibrary(xmlhttp);
    }
  };
  xmlhttp.open("GET", corpInfo.generateApiUrl('Blueprints'), true);
  xmlhttp.send();
}

function listBPOLibrary(xml){
  var i;
  var xmlDoc = xml.responseXML;
  var x = xmlDoc.getElementsByTagName("row");
  var ownedBPs = [];
  var BPTypeID;
  var BPColor;
  
  var i;
  var ids = [];
  for (i in corpLocationIds) {
    ids.push(i);
  }
  for (i = 0; i < x.length; i++) { 
    BPTypeID = x[i].getAttribute('typeID');
    // console.log(BPTypeID);
    ownedBPs.push(BPTypeID);
  }
  
  var tablaBoby="<tr><th>Kép</th><th>Blueprint</th><th>ME</th><th>TE</th></tr>";
  for (i = 0; i < BPOLib.length; i++) { 
    BPTypeID = BPOLib[i];
    if (ownedBPs.indexOf(String(BPTypeID)) == -1) {BPColor = "BPO"} else {BPColor = "BPC"};
    tablaBoby += '<tr class="' + BPColor + '">'
      + addTableData('')
      + addTableData(BlueprintNameWithLink(BPTypeID, BPColor))
      + addTableData(catBPs[BPTypeID].hiearchy)
      + addTableData('')
      + "<tr>\n";
  }
  document.getElementById("resultTable").innerHTML = tablaBoby;
}

function DispCorpInfo() {
  	
}

function BlueprintNameWithLink(BPId, BPClass) {
  return htmlLink(catBPs[BPId].name, 'http://games.chruker.dk/eve_online/item.php?type_id=' + BPId, BPClass);
}

function htmlLink(linkBody, linkHref, linkClass) {
  return '<a class="' + linkClass + '" href="' + linkHref + '">' + linkBody + '</a>'; 	 
}

function corpInfoRefresh() {
  var xmlhttp = new XMLHttpRequest();
  getAndProcessHttpRequest(corpInfo.generateApiUrl('CorporationSheet'), processCorporationSheetXml(xmlhttp) );
}

function getAndProcessHttpRequest(pUrl, processFunc) {
  document.getElementById("apiUrl").innerHTML = pUrl;
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      processFunc.call(xmlhttp, xmlhttp);
    }
  };
  xmlhttp.open("GET", pUrl, true);
  xmlhttp.send();
}

function processCorporationSheetXml(xml) {
  var i;
  var xmlDoc = self.responseXML;
  var x = xmlDoc.getElementsByTagName("result");
  var ceg = "";
// <eveapi version="2"><currentTime>2016-05-20 10:42:06</currentTime><result><corporationID>98429458</corporationID><corporationName>Fruzsi Corp</corporationName><ticker>FR-ZS</ticker><ceoID>95438298</ceoID><ceoName>Fruzsina Aldent</ceoName><stationID>60001006</stationID><stationName>Shemah VII - Moon 10 - Kaalakiota Corporation Factory</stationName><description/><url>http://</url><allianceID>0</allianceID><factionID>0</factionID><taxRate>0.5</taxRate><memberCount>4</memberCount><memberLimit>20</memberLimit><shares>1000</shares>
  ceg = x[i].getElementsByName('corporationName')[0].childNodes[0].nodeValue;
  document.getElementById("debugMess").innerHTML = ceg;
}

function processCharacterXml(xml) {
// <row characterID="95001353" name="Samo Hakoke">	
  var i;
  var xmlDoc = xml.responseXML;
  var x = xmlDoc.getElementsByTagName("row");
  var memberStr = "";
  memberStr = formatCharacterStr(x[i].getAttribute('characterID'), x[i].getAttribute('name'));
  for (i = 1; i <x.length; i++) {
    memberStr += ", " + formatCharacterStr(x[i].getAttribute('characterID'), x[i].getAttribute('name'));
  }
  
}

function formatCharacterStr(charId, charName) {
  return charName;	
}

